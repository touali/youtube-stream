# Youtube Stream (WIP)

Youtube Stream is a Drupal 8 module that fetch active stream and display it into an iframe.

## Usage

Install and enable this module the way you like.
Add the block where you want and add the required configuration.
(You'll need a Youtube Data API Key)

### Composer install

Past this in your composer.json repositories 
    
    {
        "type": "vcs",
        "url": "https://bitbucket.org/touali/youtube_stream"
    }


And then
    
    composer require touali/youtube_stream

## TODO

* Make it work :)
* Code cleanup & improvement
* Add checkboxes to improve control on iframe attributes (allowfullscreen, autoplay...)