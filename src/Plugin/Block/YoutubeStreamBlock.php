<?php

namespace Drupal\youtube_stream\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\youtube_stream\Services\YoutubeManager;

/**
 *
 * Provide a 'youtube-stream' block.
 *
 * Class YoutubeStreamBlock
 * @package Drupal\youtube_stream\Plugin\Block
 *
 * @Block(
 *     id = "youtube_stream_block",
 *     admin_label = @Translation("Youtube Stream Block"),
 *     category = @Translation("Custom")
 * )
 */
class YoutubeStreamBlock extends BlockBase implements BlockPluginInterface
{
    /**
     * {@inheritdoc}
     */
    public function defaultConfiguration() {
        $defaultConfig = \Drupal::config('youtube_stream.settings');

        return [
            'youtube_stream_channel_id',
            'youtube_stream_api_key',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function blockForm($form, FormStateInterface $form_state){
        $form = parent::blockForm($form, $form_state);

        $config = $this->getConfiguration();

        $form['youtube_stream_channel_id'] = [
            '#type' => 'textfield',
            '#title' => $this->t('Youtube Channel ID'),
            '#description' => $this->t('Your youtube Channel ID'),
            '#default_value' => isset($config['youtube_stream_channel_id']) ? $config['youtube_stream_channel_id'] : '',
            '#required' => true,
        ];

        $form['youtube_stream_api_key'] = [
            '#type' => 'textfield',
            '#title' => $this->t('Youtube API Key'),
            '#description' => $this->t('The API Key provided by Youtube'),
            '#default_value' => isset($config['youtube_stream_api_key']) ? $config['youtube_stream_api_key'] : '',
            '#required' => true,
        ];

        return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function blockSubmit($form, FormStateInterface $form_state) {
        parent::blockSubmit($form, $form_state);

        $this->configuration['youtube_stream_channel_id'] = $form_state->getValue('youtube_stream_channel_id');
        $this->configuration['youtube_stream_api_key'] = $form_state->getValue('youtube_stream_api_key');
    }

    /**
     * {@inheritdoc}
     */
    public function build() {
        $config = $this->getConfiguration();
        $youtubeManager = new YoutubeManager($config);

        $videoId = $youtubeManager->getCurrentStream();

        return [
            '#theme' => 'youtube_stream',
            '#videoId' => $videoId,
            '#cache' => [
                'max-age' => 0,
            ],
        ];
    }
}