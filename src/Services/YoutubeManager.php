<?php

namespace Drupal\youtube_stream\Services;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;

class YoutubeManager
{
    const YOUTUBE_API_URL = 'https://www.googleapis.com/youtube/v3/search?part=snippet&channelId={chanelId}&eventType=live&type=video&key={apiKey}';

    protected $channelId;

    protected $apiKey;

    /**
     * YoutubeManager constructor.
     * @param $config
     */
    public function __construct($config) {
        $this->channelId = $config['youtube_stream_channel_id'];
        $this->apiKey = $config['youtube_stream_api_key'];
    }

    /**
     * @return null
     */
    public function getCurrentStream() {
        $data = json_decode($this->getData());
        $videoId = null;

        foreach ($data->items as $item) {
            if ($item->snippet->liveBroadcastContent === 'live') {
                $videoId = $item->id->videoId;
            }
        }

        return $videoId;
    }

    /**
     * @return mixed
     */
    private function getData() {
        $url = str_replace(['{chanelId}', '{apiKey}'], [$this->channelId, $this->apiKey], static::YOUTUBE_API_URL);
        $resource = curl_init();
        curl_setopt($resource, CURLOPT_URL, $url);
        curl_setopt($resource, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($resource, CURLOPT_USERAGENT, 'Terraltitude (www.terraltitude.com)');
        $result = curl_exec ($resource);
        curl_close($resource);

        return $result;
    }
}

